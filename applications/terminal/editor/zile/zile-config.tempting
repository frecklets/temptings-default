---
doc:
  short_help: "Configuration for the 'zile' text editor"

args:
  var_name:
    doc:
      short_help: "The config variable name."
    type: string
    required: true
  var_value:
    doc:
      short_help: "The config variable value."
    type: string
    required: true
  vars_dict:
    doc:
      short_help: a map of config key value pairs
    type: dict
    required: true
    cli:
        enabled: true
  key_bindings:
    required: false
    doc:
      short_help: "Rebind keys."
    type: dict
    default: {}
    empty: true
    cli:
      show_default: false
  inhabit_splash_screen:
    doc:
      short_help: whether to inhibit the startup screen
      help: |
        Non-nil inhibits the startup screen.
        It also inhibits display of the initial message in the `*scratch*' buffer.
        Default value is nil.
    type: boolean
    default: false
  standard_indent:
    doc:
      short_help: default number of columns for margin-changing functions to indent
    type: integer
    min: 0
    default: 4
  tab_width:
    doc:
      short_help: distance between tab stops
      help: |
        Distance between tab stops (for display of tab characters), in columns.
        Default value is 8.
    type: integer
    min: 0
    default: 8
  tab_always_indent:
    doc:
      short_help: controls the operation of the TAB key
      help: |
        Controls the operation of the TAB key.
        If t, hitting TAB always just indents the current line.
        If nil, hitting TAB indents the current line if point is at the
        left margin or in the line's indentation, otherwise it inserts a
        "real" TAB character.
        Default value is t.
    type: boolean
    default: true
  indent_tabs_mode:
    doc:
      short_help: use tabs or whitespaces
      help: |
        If non-nil, insert-tab inserts "real" tabs; otherwise, it always inserts
        spaces.
        Default value is t.
    type: boolean
    default: true
  fill_column:
    doc:
      short_help: column beyond which automatic line-wrapping should happen
      help: |
        Column beyond which automatic line-wrapping should happen.
        Automatically becomes buffer-local when set in any fashion.
        Default value is 70.
    type: integer
    min: 0
    default: 70
  auto_fill_mode:
    doc:
      short_help: enable auto-filll-mode
      help: |
        If non-nil, Auto Fill Mode is automatically enabled.
        Default value is nil.
    default: false
    type: boolean
  kill_whole_line:
    doc:
      short_help: whether 'kill-line' with no arg starts at the beginning of line
      help: |
        If non-nil, `kill-line' with no arg at beg of line kills the whole line.
        Default value is nil.
    type: boolean
    default: false
  case_fold_search:
    doc:
      short_help: "'true' means searches ignore case"
      help: |
        Non-nil means searches ignore case.
        Default value is t.
    type: boolean
    default: true
  case_replace:
    type: boolean
    doc:
      short_help: "'true' means 'query-replace' shold preserve case in replacements"
    default: true
  ring_bell:
    doc:
      short_help: "'true' means ring the terminal bell on any error"
    type: boolean
    default: true
  highlight_nonselected_windows:
    doc:
      short_help: if 'true', highlight region even in nonselected windows
    type: boolean
    default: false
  make_backup_files:
    doc:
      short_help: "'true' means make a backup of a file the first time it is saved."
      help: |
        Non-nil means make a backup of a file the first time it is saved.
        This is done by appending `~' to the file name.
        Default value is t.
    type: boolean
    default: true
  backup_directory:
    doc:
      short_help: the directory for backup files, which must exist
      help: |
        The directory for backup files, which must exist.
        If this variable is nil, the backup is made in the original file's
        directory.
        This value is used only when `make-backup-files' is t.
        Default value is nil.
    type: string
    default: "nil"
  global_set_keys:
    required: false
    doc:
      short_help: rebound keys
    type: dict
    schema:
      key:
        type: string
      func:
        type: string
---
;;;; .zile configuration

;; Rebind keys with:
;; (global-set-key "key" 'func)
{%:: for key, value in key_bindings.items() ::%}
(global-set-key "{{:: key ::}}" '{{:: value ::}})
{%:: endfor ::%}

; Non-nil inhibits the startup screen.
; It also inhibits display of the initial message in the `*scratch*' buffer.
; Default value is nil.
(setq inhibit-splash-screen {{:: inhabit_splash_screen | string_for_boolean('t', 'nil') ::}})

; Default number of columns for margin-changing functions to indent.
; Default value is 4.
(setq standard-indent {{:: standard_indent ::}})

; Distance between tab stops (for display of tab characters), in columns.
; Default value is 8.
(setq tab-width {{:: tab_width ::}})

; Controls the operation of the TAB key.
; If t, hitting TAB always just indents the current line.
; If nil, hitting TAB indents the current line if point is at the
; left margin or in the line's indentation, otherwise it inserts a
; "real" TAB character.
; Default value is t.
(setq tab-always-indent {{:: tab_always_indent | string_for_boolean('t', 'nil') ::}})

; If non-nil, insert-tab inserts "real" tabs; otherwise, it always inserts
; spaces.
; Default value is t.
(setq indent-tabs-mode {{:: indent_tabs_mode | string_for_boolean('t', 'nil') ::}})

; Column beyond which automatic line-wrapping should happen.
; Automatically becomes buffer-local when set in any fashion.
; Default value is 70.
(setq fill-column {{:: fill_column ::}})

; If non-nil, Auto Fill Mode is automatically enabled.
; Default value is nil.
(setq auto-fill-mode {{:: auto_fill_mode | string_for_boolean('t', 'nil') ::}})

; If non-nil, `kill-line' with no arg at beg of line kills the whole line.
; Default value is nil.
(setq kill-whole-line {{:: kill_whole_line | string_for_boolean('t', 'nil') ::}})

; Non-nil means searches ignore case.
; Default value is t.
(setq case-fold-search {{:: case_fold_search | string_for_boolean('t', 'nil') ::}})

; Non-nil means `query-replace' should preserve case in replacements.
; Default value is t.
(setq case-replace {{:: case_replace | string_for_boolean('t', 'nil') ::}})

; Non-nil means ring the terminal bell on any error.
; Default value is t.
(setq ring-bell {{:: ring_bell | string_for_boolean('t', 'nil') ::}})

; If non-nil, highlight region even in nonselected windows.
; Default value is nil.
(setq highlight-nonselected-windows {{:: highlight_nonselected_windows | string_for_boolean('t', 'nil') ::}})

; Non-nil means make a backup of a file the first time it is saved.
; This is done by appending `~' to the file name.
; Default value is t.
(setq make-backup-files {{:: make_backup_files | string_for_boolean('t', 'nil') ::}})

; The directory for backup files, which must exist.
; If this variable is nil, the backup is made in the original file's
; directory.
; This value is used only when `make-backup-files' is t.
; Default value is nil.
(setq backup-directory {{:: backup_directory ::}})



